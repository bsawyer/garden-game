let rows = 5;
let columns = 5;

const urlParams = new URLSearchParams(window.location.search);
rows = urlParams.get('rows') || rows;
columns = urlParams.get('columns') || columns;

let playing = false;
let dropIndex = null;
let score = 0;
let finalScore = null;
let scores = [];

const items = [{
  name: 'flower',
  originTile: {
    buff: [{name: 'dirt', type: 'tile'}, {name: 'grass', type: 'tile'}],
    debuff: [{name: 'water', type: 'tile'}]
  },
  porximityTiles: {
    buff: [{name: 'water', type: 'tile'}],
    debuff: [{name: 'grass', type: 'tile'}]
  },
  allowableItems: []
},{
  name: 'butterfly',
  originTile: {
    buff: [{name: 'grass', type: 'tile'}],
    debuff: [{name: 'water', type: 'tile'}, {name: 'dirt', type: 'tile'}]
  },
  porximityTiles: {
    buff: [{name: 'flower', type: 'item'}, {name: 'grass', type: 'tile'}],
    debuff: []
  },
  allowableItems: [{
    type: 'item',
    name: 'flower'
  }]
},{
  name: 'clover',
  originTile: {
    buff: [{name: 'grass', type: 'tile'}],
    debuff: [{name: 'water', type: 'tile'}, {name: 'dirt', type: 'tile'}]
  },
  porximityTiles: {
    buff: [],
    debuff: []
  },
  allowableItems: []
}, {
  name: 'bunny',
  originTile: {
    buff: [{name: 'grass', type: 'tile'}],
    debuff: [{name: 'water', type: 'tile'}, {name: 'dirt', type: 'tile'}]
  },
  porximityTiles: {
    buff: [],
    debuff: []
  },
  allowableItems: [{
    type: 'item',
    name: 'clover'
  }]
}, {
  name: 'fish',
  originTile: {
    buff: [{name: 'water', type: 'tile'}],
    debuff: [{name: 'grass', type: 'tile'}, {name: 'dirt', type: 'tile'}]
  },
  porximityTiles: {
    buff: [],
    debuff: []
  },
  allowableItems: []
},{
  name: 'worm',
  originTile: {
    buff: [{name: 'dirt', type: 'tile'}, {name: 'grass', type: 'tile'}],
    debuff: [{name: 'water', type: 'tile'}]
  },
  porximityTiles: {
    buff: [{name: 'dirt', type: 'tile'}],
    debuff: [{name: 'water', type: 'tile'}]
  },
  allowableItems: []
},{
  name: 'bird',
  originTile: {
    buff: [{name: 'grass', type: 'tile'}],
    debuff: [{name: 'water', type: 'tile'}]
  },
  porximityTiles: {
    buff: [],
    debuff: []
  },
  allowableItems: [{
    type: 'item',
    name: 'worm'
  }]
},{
  name: 'bear',
  originTile: {
    buff: [{name: 'grass', type: 'tile'}],
    debuff: [{name: 'water', type: 'tile'}]
  },
  porximityTiles: {
    buff: [],
    debuff: []
  },
  allowableItems: [{
    type: 'item',
    name: 'fish'
  }]
}];

const tileTypes = {
  grass: 100,
  dirt: 200,
  water: 300
};

//debuff applied if placed on

const grid = document.querySelector('grid');
const startBtn = document.querySelector('start');
const pauseBtn = document.querySelector('pause');
const stopBtn = document.querySelector('stop');
const tileTypeButtons = document.querySelectorAll('.tile-types li');
const scoreElm = document.querySelector('score');
const scoreBoardElm = document.querySelector('scoreboard');
const scoreb = document.querySelector('scoreb');
const dropElm = document.querySelector('drop');
const bubbleBtn = document.querySelector('bubble');

Array.prototype.forEach.call(tileTypeButtons, (li)=>{
  li.addEventListener('click', activateType)
});

startBtn.addEventListener('click', start);
pauseBtn.addEventListener('click', pause);
stopBtn.addEventListener('click', reset);
scoreb.addEventListener('click', showScoreBoard);
bubbleBtn.addEventListener('click', switchItem);

createGrid();
populateDropItems();
updateScoreBoard();

function createGrid(){
  let i = 0;
  while(i < rows){
    let c = 0;
    let row = document.createElement('row');
    while(c < columns){
      let tile = document.createElement('tile');
      tile.innerHTML = `
      <icon class="icon-grid">
        <svg>
          <use xlink:href="#icon-bunny"></use>
        </svg>
      </icon>
      `;
      row.appendChild(tile);
      tile.setAttribute('style', `padding-top: ${100 / columns}%;`);
      tile.addEventListener('click', tileClick);
      c++;
    }
    grid.appendChild(row);
    i++;
  }
}

function populateDropItems(){
  let i = 0;
  while(i < items.length){
    let elm = document.createElement('icon');
    elm.innerHTML = `
      <svg>
        <use xlink:href="#icon-${items[i].name}"></use>
      </svg>
    `;
    elm.classList.add('icon');
    elm.classList.add(`icon-${items[i].name}`);
    // elm.setAttribute('data-name', items[i].name);
    dropElm.appendChild(elm);
    i++;
  }
}

function activateType(e){
  Array.prototype.forEach.call(tileTypeButtons, (li)=>{
  li.classList.remove('active');
});
  e.currentTarget.classList.add('active');
}

function getActiveTileType(){
  return document.querySelector('.active').getAttribute('tile-type');
}

function canItemTakePlace(item, targetTile){
  let targetTileItemName;
  if(targetTile.classList.contains('filled')){
    targetTileItemName = targetTile.querySelector('use').getAttribute('xlink:href').replace('#icon-', '');
    if(!item.allowableItems.length){
      return false;
    }
    return item.allowableItems.some((a)=>{
      return a.name === targetTileItemName;
    });
  }
  return true;
}

function tileClick(e){
  if(playing === false){
    e.currentTarget.setAttribute('tile-type', getActiveTileType());
    let placeSound = new Audio('place_sound.mp3');
    placeSound.play();
  }else{
    // if(e.currentTarget.classList.contains('filled')){
    //   let noSound = new Audio('nope_sound.mp3');
    //   noSound.play();
    //   return;
    // }
    if(!canItemTakePlace(items[dropIndex], e.currentTarget)){
      let noSound = new Audio('nope_sound.mp3');
      noSound.play();
      return;
    }
    const filledMultiplier = e.currentTarget.classList.contains('filled') ? 2 : 1;
    let clickSound = new Audio('click_3.mp3');
    clickSound.play();
    e.currentTarget.classList.add('filled');
    e.currentTarget.querySelector('use').setAttribute('xlink:href', `#icon-${items[dropIndex].name}`);
    updateScore(e.currentTarget, dropIndex, filledMultiplier);
    dropIndex = null;
    isGameOver();
  }
}

function switchItem(){
  if(playing && dropIndex !== null){
    let us = bubbleBtn.querySelector('use');
    let si = us.getAttribute('data-index');
    us.setAttribute('data-index', dropIndex);
    us.setAttribute('xlink:href', `#icon-${items[dropIndex].name}`);
    if(si){
      dropIndex = si;
      dropItem(dropIndex);
    }else{
      dropIndex = null;
    }
  }
}

function clearBubble(){
  let us = bubbleBtn.querySelector('use');
  us.setAttribute('data-index', '');
  us.setAttribute('xlink:href', '');
}

function placeItem(){
  let t = document.querySelector('tile:not(.filled)');
  if(t){
    t.click();
  }
}

function updateScore(tileElm, index, multiplier){
  multiplier = multiplier || 1;
  let tileName = tileElm.getAttribute('tile-type');
  let scr = tileTypes[tileName] || 0;
  let itemData = items[index];
  itemData.originTile.buff.forEach((ot)=>{
    if(ot.name === tileName){
      scr = scr + (500 * (ot.multiplier || multiplier));
    }
  });
  itemData.originTile.debuff.forEach((ot)=>{
    if(ot.name === tileName){
      scr = scr - (500 * (ot.multiplier || 1));
    }
  });
  score = score + scr;
  if(score < 0){
    score = 0;
  }
  scoreElm.textContent = numberWithCommas(score);
}

function isGameOver(){
  if(document.querySelectorAll('.filled').length >= rows * columns){
    //all done
    let doneSound = new Audio('done.mp3');
    doneSound.play();
    stop();
    finalScore = score;
    updateScoreBoard();
    showScoreBoard();
  }
}

function start(){
  if(playing) return;
  audioLoop.volume = 0.2;
  audioLoop.play();
  playing = true;
  let d = document.querySelector('.drop-pls');
  if(d){
    d.style.webkitAnimationPlayState = 'running';
  }
}

function pause(){
  playing = false;
  audioLoop.pause();
  let d = document.querySelector('.drop-pls');
  if(d){
    d.style.webkitAnimationPlayState = 'paused';
  }
}

function stop(){
  playing = false;
  audioLoop.pause();
  audioLoop.currentTime = 0;
  dropIndex = null;
  let d = document.querySelector('.drop-pls');
  clearBubble();
  if(d){
    d.classList.remove('drop-pls');
  }
}

function reset(){
  stop();
  score = 0;
  scoreElm.textContent = 0;
  Array.prototype.forEach.call(document.querySelectorAll('tile'), (t)=>{
    t.removeAttribute('tile-type');
    t.classList.remove('filled');
  });
}

function onAnimationEnd(){
  console.log('animation end');
  placeItem(dropIndex);
  dropIndex = null;
}

Array.prototype.forEach.call(document.querySelectorAll('.icon'), (elm)=>{
  elm.addEventListener('animationend', onAnimationEnd);
});

function dropItem(i){
  let d = document.querySelector('.drop-pls');
  if(d){
    d.classList.remove('drop-pls');
  }
  setTimeout(()=>{
    document.querySelector(`.icon-${items[i].name}`).classList.add('drop-pls');  
  })
}

function tick(){
  if(playing){
    if(dropIndex === null){
      dropIndex = randomIntFromInterval(0, items.length - 1);
      dropItem(dropIndex);
      // let d = document.querySelector('.drop-pls');
      // if(d){
      //   d.classList.remove('drop-pls');
      // }
      // setTimeout(()=>{
      //   document.querySelector(`.icon-${items[dropIndex].name}`).classList.add('drop-pls');  
      // })
      // 
    }
  }else{
    //derp
  }
  requestAnimationFrame(tick);
}

requestAnimationFrame(tick);

function updateScoreBoard(){
  scores = getScores() || scores;
  scoreBoardElm.innerHTML = '<div><h1>High Scores</h1><close>X</close></div>';
  scoreBoardElm.querySelector('close').addEventListener('click', hideScoreBoard);
  let appended = false;
  
  scores
  .sort((a,b) => {
    if(a.score > b.score){
      return -1;
    }else if(b.score > a.score){
      return 1;
    }
    return 0;
  })
  .forEach((s)=>{
    let d = document.createElement('div');
    if(finalScore && finalScore >= s.score && !appended){
      appended = true;
      let dd = document.createElement('div');
      dd.innerHTML = `
      <h4><input type="text" placeholder="NAME"/></h4>
      <p>${numberWithCommas(finalScore)}</p>
      `;
      dd.querySelector('input').addEventListener('keydown', (e)=>{
        if(e.keyCode === 13){
          scores.push({
            name: e.currentTarget.value.toUpperCase(),
            score: finalScore
          });
          finalScore = null;
          saveScores(scores);
          updateScoreBoard();
        }
      });
      scoreBoardElm.appendChild(dd);
    }
    d.innerHTML = `
    <h4>${s.name}</h4>
    <p>${numberWithCommas(s.score)}</p>
    `;
    scoreBoardElm.appendChild(d);
  });
  
  if(!appended && finalScore){
    let d = document.createElement('div');
    d.innerHTML = `
    <h4><input type="text" placeholder="NAME"/></h4>
    <p>${numberWithCommas(finalScore)}</p>
    `;
    d.querySelector('input').addEventListener('keydown', (e)=>{
      if(e.keyCode === 13){
        scores.push({
          name: e.currentTarget.value.toUpperCase(),
          score: finalScore
        });
        finalScore = null;
        saveScores(scores);
        updateScoreBoard();
      }
    });
    scoreBoardElm.appendChild(d);
  }
}

function getScores(){
  return JSON.parse(localStorage.getItem('scores'));
}

function saveScores(records){
  localStorage.setItem('scores', JSON.stringify(records));
}

function showScoreBoard(){
  scoreBoardElm.classList.add('show');
}

function hideScoreBoard(){
  scoreBoardElm.classList.remove('show');
}

// min and max included
function randomIntFromInterval(min,max) {
  return Math.floor(Math.random()*(max-min+1)+min);
}

function numberWithCommas(x) {
  let parts = x.toString().split(".");
  parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  return parts.join(".");
}
